# Image Processing Tool

This tool resizes images, adds a logo, and overlays text images in different languages. The script allows you to pass arguments directly via the command line or prompts for missing arguments interactively using `questionary`. Progress is displayed using a `tqdm` progress bar.

## Features

- Resize images to specified dimensions.
- Add a logo to resized images.
- Overlay text images in different languages.
- Supports interactive prompts for missing arguments.
- Displays progress using a progress bar.

## Requirements

- https://devenv.sh/getting-started/

## Installation 
- `git clone https://gitlab.com/YoelisA/batch-image-processing.git`
- `cd batch-image-processin`
- `devenv shell`
- `python main.py --folder_path ./test/ --logo_path ./test/logo/logo.png --languages_folder ./test/languages/ --sizes 800x600,400x300,1300x1400`
or in order to use interactive CLI
- ```python main.py```