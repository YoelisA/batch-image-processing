import os
import questionary
import argparse
from tqdm import tqdm
from PIL import Image, ImageOps


def crop_to_aspect_ratio(image, target_size):
    """
    Crops an image to the specified aspect ratio, keeping the center of the image.
    """
    width, height = image.size
    target_width, target_height = target_size

    current_ratio = width / height
    target_ratio = target_width / target_height

    if current_ratio > target_ratio:
        # Crop the width
        new_width = int(target_ratio * height)
        offset = (width - new_width) // 2
        box = (offset, 0, offset + new_width, height)
    else:
        # Crop the height
        new_height = int(width / target_ratio)
        offset = (height - new_height) // 2
        box = (0, offset, width, offset + new_height)

    return image.crop(box)


def transform_image(image_path, output_path, size, logo_path, language_image_path):
    """
    Resizes an image to the specified size and saves it to the output path.
    """
    with Image.open(image_path) as img:
        img = ImageOps.exif_transpose(img)
        # Crops around the center and then resize
        # border = ((width - target_width) / 2, (height - target_height) / 2, (width - target_width) / 2, (height - target_height) / 2) # left, top, right, bottom
        # img = ImageOps.crop(img, border).resize(size)

        # Resize while preserving aspect ratio. Loses expected dimension
        # img.thumbnail(size, Image.Resampling.LANCZOS)

        # Resize without preserving aspect ratio. Stretches to match
        # img = img.resize(size)

        img = crop_to_aspect_ratio(img, size).resize(
            size, resample=Image.Resampling.LANCZOS
        )

        add_logo(img, size, logo_path)
        add_language(img, size, language_image_path)

        img.save(output_path)


def add_logo(img, size, logo_path):
    with Image.open(logo_path) as logo:
        logo = logo.convert("RGBA")
        # Resize logo to fit the resized image if necessary
        width, height = size
        RELATIVE_SIZE = 0.25
        logo_width, logo_height = (
            int(width * RELATIVE_SIZE),
            int(height * RELATIVE_SIZE),
        )
        logo.thumbnail((logo_width, logo_height))
        position = (width - logo_width, height - logo_height)
        img.paste(logo, position)


def add_language(img, size, text_image_path):
    with Image.open(text_image_path) as text_img:
        text_img = text_img.convert("RGBA")
        # Resize text image to fit if necessary
        text_img_size = (
            int(size[0] * 0.25),
            int(size[1] * 0.25),
        )  # Example size: 50% width, 10% height
        text_img = text_img.resize(text_img_size, Image.Resampling.LANCZOS)

        # Calculate position for text (e.g., top left corner)
        text_position = (
            size[0] // 2 - text_img_size[0],
            size[1] // 2,
        )  # Starting position for the text
        img.paste(text_img, text_position, text_img)


def process_images(folder_path, sizes, logo_path, languages_folder):
    """
    Resizes all images in the given folder to the specified sizes.
    """
    if not os.path.exists(folder_path):
        print(f"Folder '{folder_path}' does not exist.")
        return

    if not os.path.exists(logo_path):
        print(f"File '{logo_path}' does not exist.")
        return

    images = [
        f
        for f in os.listdir(folder_path)
        if f.lower().endswith((".png", ".jpg", ".jpeg", ".bmp", ".gif", ".tiff"))
    ]
    text_images = [
        f for f in os.listdir(languages_folder) if f.lower().endswith(".png")
    ]
    total_iterations = len(images) * len(sizes) * len(text_images)

    # Process each image in the folder

    with tqdm(total=total_iterations, desc="Processing images") as pbar:
        for filename in images:
            file_path = os.path.join(folder_path, filename)
            for size in sizes:
                for text_image_filename in text_images:
                    language = os.path.splitext(text_image_filename)[0]
                    text_image_path = os.path.join(languages_folder, text_image_filename)
                    output_folder = os.path.join(
                        folder_path, f"{size[0]}x{size[1]}", language
                    )
                    if not os.path.exists(output_folder):
                        os.makedirs(output_folder)
                    base, ext = os.path.splitext(filename)
                    output_filename = f"{base}_{language}{ext}"
                    output_path = os.path.join(output_folder, output_filename)
                    transform_image(
                        file_path, output_path, size, logo_path, text_image_path
                    )
                    pbar.set_description(f"Processing {file_path} in {language}")
                    pbar.update(1)


def get_args():
    parser = argparse.ArgumentParser(
        description="Resize images, add watermark, and overlay text images."
    )
    parser.add_argument(
        "--folder_path", type=str, help="Path to the folder containing images."
    )
    parser.add_argument("--logo_path", type=str, help="Path to the logo image.")
    parser.add_argument(
        "--languages_folder",
        type=str,
        help="Path to the folder containing languages text images.",
    )
    parser.add_argument(
        "--sizes", type=str, help="Output sizes (e.g., 800x600,400x300)."
    )
    return parser.parse_args()


def prompt_for_missing_args(args):
    if not args.folder_path:
        args.folder_path = questionary.path(
            "Please enter the folder path containing images:"
        ).ask()
        while not os.path.isdir(args.folder_path):
            args.folder_path = questionary.path(
                "Invalid path. Please enter a valid folder path containing images:"
            ).ask()

    if not args.logo_path:
        args.logo_path = questionary.path(
            "Please enter the path to the logo image:"
        ).ask()
        while not os.path.isfile(args.logo_path):
            args.logo_path = questionary.path(
                "Invalid path. Please enter a valid path to the logo image:"
            ).ask()

    if not args.languages_folder:
        args.languages_folder = questionary.path(
            "Please enter the folder path containing text images:"
        ).ask()
        while not os.path.isdir(args.languages_folder):
            args.languages_folder = questionary.path(
                "Invalid path. Please enter a valid folder path containing text images:"
            ).ask()

    if not args.sizes:
        args.sizes = questionary.text(
            "Please enter the output sizes (e.g., 800x600,400x300):"
        ).ask()

    return args


def main():
    args = get_args()
    args = prompt_for_missing_args(args)
    sizes_list = [tuple(map(int, size.split("x"))) for size in args.sizes.split(",")]
    process_images(args.folder_path, sizes_list, args.logo_path, args.languages_folder)


if __name__ == "__main__":
    main()
